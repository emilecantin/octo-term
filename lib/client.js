const axios = require('axios');
const SockJs = require('sockjs-client');

const defaultConfig = {
  host: '',
  apiKey: '',
}

class OctoClient {
  constructor(_config) {
    this.config = Object.assign({}, defaultConfig, _config);
    const baseURL = `http://${this.config.host}/api/`;
    this.baseURL = baseURL;
    this.a = axios.create({
      baseURL,
      headers: {
        common: {
          'X-Api-Key': this.config.apiKey
        }
      }
    });
  }
  subscribe(callback) {
    this.socket = new SockJs( `http://${this.config.host}/sockjs`);
    // this.socket.onopen = () => console.log('open');
    this.socket.onmessage = callback;
    // this.socket.onclose = () => console.log('close');
  }
  async get(path, params) {
    const {host, apiKey} = this.config;
    const response = await this.a.get(path, {
      params
    });
    return response;
  }
}

module.exports = OctoClient;
