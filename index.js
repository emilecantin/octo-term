const blessed = require('blessed');
const contrib = require('blessed-contrib');
const OctoClient = require('./lib/client');

// Config
const config = {
  host: '192.168.1.174',
  apiKey: '3BDA72AC4057453C9E9B39F3142A75AF',
};
const octoClient = new OctoClient(config);

const screen = blessed.screen();
const line = contrib.line({
  style: {
    line: "yellow",
    text: "green",
    baseline: "black"
  },
  xLabelPadding: 3,
  xPadding: 5,
  label: 'Temps',
  showLegend: true,
  minY: 0,
  maxY: 300,
});
screen.append(line); //must append before setting data

const lineData = [{
  title: 'Bed - Actual',
  style: {
    line: 'blue'
  },
  x: [],
  y: [],
}, {
  title: 'Bed - Target',
  style: {
    line: 'blue'
  },
  x: [],
  y: [],
}, {
  title: 'Tool- Actual',
  style: {
    line: 'red'
  },
  x: [],
  y: [],
}, {
  title: 'Tool- Target',
  style: {
    line: 'red'
  },
  x: [],
  y: [],
}];

function addLineData(entry) {
  lineData[0].y.push(entry.bed.actual);
  lineData[0].x.push(entry.time);

  lineData[1].y.push(entry.bed.target);
  lineData[1].x.push(entry.time);

  lineData[2].y.push(entry.tool0.actual);
  lineData[2].x.push(entry.time);

  lineData[3].y.push(entry.tool0.target);
  lineData[3].x.push(entry.time);
}
octoClient.get('printer', {history: true, limit: 20})
.then(res => {
  res.data.temperature.history.forEach(addLineData);
  // console.log(lineData);
  line.setData(lineData);
  screen.render();
  // console.log(res.data.temperature.history);
});
octoClient.subscribe(e => {
  if(e.data.current) {
    // console.log('data', e.data.current.temps);
    e.data.current.temps.forEach(addLineData);
    line.setData(lineData);
    screen.render();
  }
});

screen.key(['escape', 'q', 'C-c'], function(ch, key) {
  return process.exit(0);
});

screen.render();
